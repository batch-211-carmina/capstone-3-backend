import bcrypt from 'bcryptjs';

const data = {
  users: [
    {
      firstName: 'John',
      lastName: 'Doe',
      mobileNo: '09201112222',
      email: 'user@mail.com',
      password: bcrypt.hashSync('user123'),
      isAdmin: false,
    },
    {
      firstName: 'Carmaj',
      lastName: 'Swittut',
      mobileNo: '09991112222',
      email: 'admin@mail.com',
      password: bcrypt.hashSync('admin123'),
      isAdmin: true,
    },
  ],
  products: [
    {
      // _id: '1',
      name: 'Choco Overload Cake',
      slug: 'choco-overload-cake',
      category: 'Cake',
      image: '/images/chocoload.png',
      price: 1200,
      countInStock: 10,
      rating: 3,
      numReviews: 5,
      description:
        'Layers of different textures of chocolate cake with various chocolate fillings and toppings.',
    },
    {
      // _id: '2',
      name: 'Mango Bravo Cake',
      slug: 'mango-bravo-cake',
      category: 'Cake',
      image: '/images/mangobravo.png',
      price: 1800,
      countInStock: 15,
      rating: 5,
      numReviews: 20,
      description:
        'Layers of crunchy wafers filled with chocolate mousse, cream and mango cubes.',
    },
    {
      // _id: '3',
      name: 'Ube Cake',
      slug: 'ube-cake',
      category: 'Cake',
      image: '/images/ubecake.png',
      price: 1300,
      countInStock: 0,
      rating: 4.2,
      numReviews: 14,
      description:
        'Ube sponge cake with real purple yam and layers of creamy icing on top.',
    },
    {
      // _id: '4',
      name: 'Vanilla Chiffon Cake',
      slug: 'vanilla-chiffon-cake',
      category: 'cake',
      image: '/images/vanillachiffon.png',
      price: 1000,
      countInStock: 10,
      rating: 3.5,
      numReviews: 10,
      description:
        'A light, spongy texture and fluffy with an open, “chiffon” grain, and a delicate flavor.',
    },
    {
      // _id: '5',
      name: 'Autumn Cake',
      slug: 'autumn-cake',
      category: 'cake',
      image: '/images/autumncake.png',
      price: 1100,
      countInStock: 10,
      rating: 4.6,
      numReviews: 8,
      description:
        'A fluffy, spongy, and moist chocolate cake filled with autumn decoration.',
    },
    {
      // _id: '6',
      name: 'Ube Ensaymada',
      slug: 'ube-ensaymada',
      category: 'bread',
      image: '/images/ubemada.png',
      price: 80,
      countInStock: 10,
      rating: 5,
      numReviews: 15,
      description:
        'Soft, sweet dough pastry covered with butter and sugar then topped with lots of grated cheese and ube jam.',
    },
    {
      // _id: '7',
      name: 'Cheesy Ensaymada',
      slug: 'cheesy-ensaymada',
      category: 'Dessert',
      image: '/images/cheesymada.png',
      price: 80,
      countInStock: 5,
      rating: 4.1,
      numReviews: 8,
      description:
        'Soft, sweet dough pastry covered with butter and sugar then topped with lots of grated cheese.',
    },
    {
      // _id: '8',
      name: 'Red Velvet Cupcake',
      slug: 'red-velvet-cupcake',
      category: 'cupcake',
      image: '/images/redvelvet.png',
      price: 130,
      countInStock: 10,
      rating: 5,
      numReviews: 15,
      description:
        'A very dramatic looking cake with a bright red colour contrasted with white icing.',
    },
  ],
};

export default data;
